<li>
    <a href="">
        <i class="fas fa-file-alt"></i> Posts
        <i class="fa arrow"></i>
    </a>
    <ul class="sidebar-nav mt-0">
        <li>
            <a href="{{url(\BlogJp\Classes\Utility\ManageUrl::post('create'))}}"> Cadastrar </a>
        </li>
        <li>
            <a href="{{url(\BlogJp\Classes\Utility\ManageUrl::post('list'))}}"> Listar </a>
        </li>
    </ul>
</li>
<li>
    <a href="">
        <i class="fas fa-tags"></i> Categorias
        <i class="fa arrow"></i>
    </a>
    <ul class="sidebar-nav mt-0">
        <li>
            <a href="{{url(\BlogJp\Classes\Utility\ManageUrl::category('create'))}}"> Cadastrar </a>
        </li>
        <li>
            <a href="{{url(\BlogJp\Classes\Utility\ManageUrl::category('list'))}}"> Listar </a>
        </li>
    </ul>
</li>
<li>
    <a href="">
        <i class="fas fa-file-alt"></i> NewsLetter
        <i class="fa arrow"></i>
    </a>
    <ul class="sidebar-nav mt-0">
        <li>
            <a href="{{url(\BlogJp\Classes\Utility\ManageUrl::post('list-newsletter'))}}"> Listar </a>
        </li>
    </ul>
</li>