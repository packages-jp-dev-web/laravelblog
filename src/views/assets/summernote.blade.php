@section('css')
@parent
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.41.0/codemirror.min.css" />
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.41.0/theme/monokai.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.44.0/addon/hint/show-hint.css" />
<style>
    .note-editor .btn{
        color: black!important;
    }
    .note-popover .btn{
        color: black!important;
    }
    .note-toolbar{
        display: block!important;
    }
</style>
@stop
@section('js')
@parent
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.41.0/codemirror.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.44.0/addon/hint/show-hint.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.44.0/addon/hint/xml-hint.min.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.44.0/addon/hint/html-hint.min.js'></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/codemirror/5.41.0/mode/xml/xml.min.js"></script>  
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.44.0/mode/javascript/javascript.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.44.0/mode/css/css.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.10.0/mode/htmlmixed/htmlmixed.min.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-bs4.js"></script>
<script src="{{url('assets/blogjp/summernote-pt-BR.js')}}"></script>
@stop
@section('js-util')
@parent
$('{{$summernote}}').on('summernote.init', function () {
if($('{{$summernote}}').summernote("code") == '<p><br></p>'){
    $('{{$textarea}}').html('');
}else{
$('{{$textarea}}').html($('{{$summernote}}').summernote("code"));
}
}).on("summernote.blur", function () {
if($('{{$summernote}}').summernote("code") == '<p><br></p>'){
$('{{$textarea}}').html('');
}else{
$('{{$textarea}}').html($('{{$summernote}}').summernote("code"));
}
}).summernote({
enterHtml: '',
placeholder: 'Entre com o conteúdo de seu post',
tabsize: 2,
height: 300,
lang: 'pt-BR',
codemirror: {
mode: 'text/html',
htmlMode: true,
lineNumbers: true,
theme: 'monokai',
tabMode: 'indent',
extraKeys: {"Ctrl-Space": "autocomplete"},
value: document.documentElement.innerHTML
}
});
@stop