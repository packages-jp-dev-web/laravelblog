@component('mail::message', ['header_url' => $header_url, 'header_title' => $header_title])
{!!$context!!}
@component('mail::button', ['url' => url('/')])
Ver mais publicações
@endcomponent
<?php
$url = url(BlogJp\Classes\Utility\ManageUrl::post('cancel-newsletter')
        .\UtilitiesJp\ClassesUtilitys\EncryptUtility::numHash($newsletter->id));
?>
Para cancelar a incrisção em nossa newsletter, <a href="{{$url}}">clique aqui.</a>
@endcomponent
