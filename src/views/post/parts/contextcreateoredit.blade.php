<article class="content forms-page">
    <div class="title-block">
        <div class="row">
            <div class="col-8">
                @if($post)
                <h3 class="title"> Editar Post </h3>
                @else
                <h3 class="title"> Cadastrar Post </h3>
                @endif                
                <p class=" text-danger mt-2"> Campos obrigatórios (*). </p>
            </div>
            <div class="col-4 pl-0 text-right">
                <a href="" class="btn btn-primary back_home"><i class="fa fa-angle-double-left mr-2"></i>Voltar</a>
            </div>
        </div>                
    </div>
    <section class="section">
        <div class="row sameheight-container">
            @if(session('success'))
            <div class="col-12">
                <div class=" card card-success">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('success')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(session('error'))
            <div class="col-12">
                <div class=" card card-danger">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('error')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-12">
                <?php
                $url = 'create';
                if ($post) {
                    $url = 'edit';
                }
                $key = key($errors->messages());
                ?>
                @if ($key)
                @section ('js-util')
                @parent
                $('#form-post [name={{$key}}]') . focus();
                @stop
                @endif
                <div class="card card-block sameheight-item" style="height: 720px;">       
                    <form id="form-post" action="{{url(BlogJp\Classes\Utility\ManageUrl::post($url))}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if($post) 
                        <input name="slug" type="hidden" value='{{$post->slug}}'>
                        @endif
                        <div class="form-group row">
                            <div class="col-12 my-2">
                                <label class="control-label">Título * </label>
                                <input name='title' type="text" class="form-control underlined" placeholder="Forneça um título para o post." @if($post) value="{{old('title',$post->title)}}" @else value="{{old('title')}}" @endif> 
                                       <small class="text-danger">{{$errors->first('title')}}</small>                                    
                            </div>
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Categoria * </label>
                                <?php
                                $category_id = null;
                                $status = null;
                                if ($post) {
                                    $category_id = $post->category_id;
                                    $status = $post->status;
                                }
                                ?>
                                {{Form::select('category_id',[''=>'Escolha uma categoria...']+$categories,old('category_id',$category_id),array('class'=>'form-control underlined'))}}
                                <small class="text-danger">{{$errors->first('category_id')}}</small>                                    
                            </div>
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Status  </label>
                                {{Form::select('status',[true=>'Ativo',false=>'Inativo'],old('status',$status),array('class'=>'form-control underlined'))}}                                                                 
                            </div>
                            <div class="col-12 my-2">
                                <label class="control-label">Descrição * </label>
                                <textarea name='description' type="text" class="form-control underlined" placeholder="Escreva uma breve descrição do post." >@if($post){{old('description',$post->description)}}@else{{old('description')}}@endif</textarea> 
                                <small class="text-danger">{{$errors->first('description')}}</small>                                    
                            </div>
                            <div class="col-12 my-2" style="color: black!important;">
                                <label class="control-label">Conteúdo * </label>
                                <div id="summernote" class="summernote">@if($post){!!old('content',$post->content)!!}@else{!!old('content')!!}@endif</div>
                                <textarea id="textarea_summernote" name='content' hidden="" class="d-none" ></textarea>
                                <small class="text-danger">{{$errors->first('content')}}</small>                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <button dusk="save" type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>            
        </div>
    </section>    
</article>
@include('blogjp::assets.summernote',['summernote'=>'#summernote','textarea'=>'#textarea_summernote'])
@section('js-util')
@parent
$('.back_home').attr('href',$('#home_panel_admin').attr('href'));
@stop