<article class="content forms-page">
    <div class="title-block">
        <div class="row">
            <div class="col-8">
                @if($category)
                <h3 class="title"> Editar Categoria </h3>
                @else
                <h3 class="title"> Cadastrar Categoria </h3>
                @endif                
                <p class="text-danger mt-2"> Campos obrigatórios (*). </p>
            </div>
            <div class="col-4 pl-0 text-right">
                <a href="" class="btn btn-primary back_home"><i class="fa fa-angle-double-left mr-2"></i>Voltar</a>
            </div>
        </div>                
    </div>
    <section class="section">
        <div class="row sameheight-container">
            @if(session('success'))
            <div class="col-12">
                <div class=" card card-success">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('success')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(session('error'))
            <div class="col-12">
                <div class=" card card-danger">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('error')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-12">
                <?php
                $url = 'create';
                if ($category) {
                    $url = 'edit';
                }
                $key = key($errors->messages());
                ?>
                @if ($key)
                @section ('js-util')
                @parent
                $('#form-category [name={{$key}}]') . focus();
                @stop
                @endif
                <div class="card card-block sameheight-item" style="height: 720px;">       
                    <form id="form-category" action="{{url(BlogJp\Classes\Utility\ManageUrl::category($url))}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @if($category) 
                        <input name="slug" type="hidden" value='{{$category->slug}}'>
                        @endif
                        <div class="form-group row">
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Nome * </label>
                                <input name='name' type="text" class="form-control underlined" placeholder="Dê um nome a sua categoria." @if($category) value="{{old('name',$category->name)}}" @else value="{{old('name')}}" @endif> 
                                       <small class="text-danger">{{$errors->first('name')}}</small>                                    
                            </div>
                            <div class="col-sm-6 my-2">
                                <label class="control-label">Descrição * </label>
                                <textarea name='description' class="form-control underlined" placeholder="Descreva em poucas palavras sua categoria.">@if($category){{old('description',$category->description)}}@else{{old('description')}}@endif</textarea>
                                <small class="text-danger">{{$errors->first('description')}}</small>                                    
                            </div>
                        </div>
                        <div class="form-group">
                            <button dusk="save" type="submit" class="btn btn-primary">Enviar</button>
                        </div>
                    </form>
                </div>
            </div>            
        </div>
    </section>    
</article>
@section('js-util')
@parent
$('.back_home').attr('href',$('#home_panel_admin').attr('href'));
@stop