<article class="content forms-page">
    <div class="title-block">
        <div class="row">
            <div class="col-8">
                <h3 class="title"> Listar Categorias do Blog </h3>
                <p class="text-danger font-weight-bold mt-2"> 
                    Atenção !!!<br>
                    Em caso da exclusão de uma categoria, os posts relacionados a mesma serão excluídos.<br>
                    Certifique - se de alterar as categorias dos posts primeiramente.
                </p>
            </div>
            <div class="col-4 pl-0 text-right">
                <a dusk="back" href="" class="btn btn-primary back_home"><i class="fa fa-angle-double-left mr-2"></i>Voltar</a>
                <a dusk="back" href="{{url(\BlogJp\Classes\Utility\ManageUrl::category('create'))}}" class="btn btn-primary"><i class="fa fa-plus mr-2"></i>Novo</a>
            </div>
        </div> 
    </div>
    <section class="section">
        <div class="row sameheight-container">
            @if(session('success'))
            <div class="col-12">
                <div class=" card card-success">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('success')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(session('error'))
            <div class="col-12">
                <div class=" card card-danger">
                    <div class="card-header">
                        <div class="header-block">
                            <p class="title text-white"> {{session('error')}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @endif            
            <div class="col-md-12">
                <div class="card">
                    <div class="card-block">
                        <div class="card-title-block">
                            <i class="fas fa-tags fa-2x"></i>
                        </div>
                        <section class="example">
                            <table id="list-categories" class="table table-striped table-bordered table-hover" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Description</th>                                       
                                        <th>Açoes</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $category)
                                    <tr>
                                        <td>{{$category->name}}</td>
                                        <td>{{$category->description}}</td>
                                        <td>
                                            <a dusk="edit-{{$category->slug}}" href="{{url(BlogJp\Classes\Utility\ManageUrl::category('edit').$category->slug)}}" class="btn btn-oval btn-info-outline"><i class="far fa-edit"></i></a>                                          
                                            <button dusk="delete-{{$category->slug}}" type="button" class="btn btn-oval btn-danger-outline open-modal-delete" data-url='{{url(BlogJp\Classes\Utility\ManageUrl::category('delete').$category->slug)}}' data-toggle='modal' data-target='#modal-delete'><i class="far fa-trash-alt"></i></button>
                                        </td>
                                    </tr>                                                                        
                                    @endforeach
                                </tbody>
                            </table>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>

@section('js-util')
@parent
$('.back_home').attr('href',$('#home_panel_admin').attr('href'));
$('body').on('click', '.open-modal-delete', function () {        
var myThis = this;
$('#button-delete-modal').attr('href', myThis.getAttribute('data-url'));
});
@stop