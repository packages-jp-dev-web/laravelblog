@extends('adminBase.template')
@section('title-complement')
Blog - Listar E-mails Newsletter
@stop
@section('context')
@include('blogjp::newsletter.parts.contextlist')
@include('adminBase.parts.modal-delete')
@stop

@section('css')
@parent
@include('adminBase.assets.css-datable')
@stop
@section('js')
@parent
@include('adminBase.assets.js-datable')
@stop
@section('js-util')
@parent
$(document).ready( function () {
$('#list-newsletter').DataTable({
responsive: true,
"order": [[ 2, "desc" ]],
"language": {
"url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
}
} );
} );
@stop