<?php

use BlogJp\Classes\Utility\ManageUrl;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['middleware' => ['web']], function () {
    $namespace = "BlogJp\Classes\Controllers\\";
    Route::prefix('blog')->group(function () use($namespace) {
        Route::get(ManageUrl::categoryNotPrefix('list'), $namespace . 'CategoryController@getList');
        Route::group(['middleware' => ['auth.authorblogjp']], function () use($namespace) {
            //Category
            Route::get(ManageUrl::categoryNotPrefix('list'), $namespace . 'CategoryController@getList');
            Route::get(ManageUrl::categoryNotPrefix('create'), $namespace . 'CategoryController@getRegister');
            Route::post(ManageUrl::categoryNotPrefix('create'), $namespace . 'CategoryController@postRegister');
            Route::get(ManageUrl::categoryNotPrefix('edit') . '{slug}', $namespace . 'CategoryController@getEdit');
            Route::post(ManageUrl::categoryNotPrefix('edit'), $namespace . 'CategoryController@postEdit');
            Route::get(ManageUrl::categoryNotPrefix('delete') . '{slug}', $namespace . 'CategoryController@getDelete');
            //Post
            Route::get(ManageUrl::postNotPrefix('list'), $namespace . 'PostController@getList');
            Route::get(ManageUrl::postNotPrefix('create'), $namespace . 'PostController@getRegister');
            Route::post(ManageUrl::postNotPrefix('create'), $namespace . 'PostController@postRegister');
            Route::get(ManageUrl::postNotPrefix('edit') . '{slug}', $namespace . 'PostController@getEdit');
            Route::post(ManageUrl::postNotPrefix('edit'), $namespace . 'PostController@postEdit');
            Route::get(ManageUrl::postNotPrefix('delete') . '{slug}', $namespace . 'PostController@getDelete');
            //NewsLetter
            Route::get(ManageUrl::postNotPrefix('list-newsletter'), $namespace . 'PostController@getListNewsLetter');
            Route::get(ManageUrl::postNotPrefix('delete-newsletter') . '{id}', $namespace . 'PostController@getDeleteNewsLetter');
            Route::get(ManageUrl::postNotPrefix('cancel-newsletter') . '{id}', $namespace . 'PostController@getCancelNewsLetter');
        });
        Route::get(ManageUrl::postNotPrefix('like-post'), $namespace . 'PostController@getLike');
        Route::post(ManageUrl::postNotPrefix('newsletter'), $namespace . 'PostController@postRegisterNewsletter');
    });
});
