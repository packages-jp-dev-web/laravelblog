<?php

namespace BlogJp;

use Illuminate\Support\ServiceProvider;

class BlogJpLaravelServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() {
        $this->loadRoutesFrom(__DIR__ . '/routes/routes.php');
        $this->loadMigrationsFrom(__DIR__ . '/migrations');
        $this->loadViewsFrom(__DIR__ . '/views', 'blogjp');
        $this->publishes([__DIR__ . "/config/blogjp.php" => config_path('blogjp.php')]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
