<?php
use Faker\Generator as Faker;
use UtilitiesJp\ClassesUtilitys\StringUtility;
$fakerCustom = \Faker\Factory::create('pt_BR');
$factory->define(BlogJp\Classes\Models\Post::class, function (Faker $faker) use($fakerCustom){ 
    $title = 'Processo '.$fakerCustom->name(50);
    return [
        'author_id'=>1,
        'slug'=> StringUtility::generateSlugOfTextWithComplement($title),
        'title'=> $title,
        'category_id'=> rand(1, 3),
        'description' => $faker->realText(250),
        'content' =>$faker->randomHtml(2,3),
        'created_at'=> $faker->dateTime('now')
    ];
});
