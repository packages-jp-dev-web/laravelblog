<?php

namespace BlogJp\Classes\Jobs\Email;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
//Services
use BlogJp\Classes\Services\NewsletterPostService;
use UtilitiesJp\Services\ContactService;
//Mail
use BlogJp\Classes\Mail\MailAfterCreatePost;

class SendEmailAfterCreatePostJob implements ShouldQueue {

    use Dispatchable,
        InteractsWithQueue,
        Queueable,
        SerializesModels;

    public $newsLetterPostService;
    public $post;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($post) {
        $this->newsLetterPostService = new NewsletterPostService();
        $this->post = $post;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {
        try {
            $newsLettersPost = $this->newsLetterPostService->getAll();
            foreach ($newsLettersPost as $newsLetterPost) {
                ContactService::sendEmail($newsLetterPost->email, new MailAfterCreatePost(['post'=>$this->post,'newsletter'=>$newsLetterPost]));
            }
        } catch (Exception $ex) {
            $logs = new \UtilitiesJp\ClassesUtilitys\LogsSystem();
            $logs->writeLogJob($ex->getMessage());
            return 1;
        }
    }

}
