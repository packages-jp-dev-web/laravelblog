<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlogJp\Classes\Services;

/**
 * Description of NewsletterPostService
 *
 * @author Jefferson
 */
//Services
use UtilitiesJp\Services\ServiceDefault;
//Utilities
use UtilitiesJp\ClassesUtilitys\LogsSystem;
//Models
use BlogJp\Classes\Models\NewsletterPost;

class NewsletterPostService extends LogsSystem implements ServiceDefault {

    //put your code here
    public function create($data) {
        return NewsletterPost::create($data);
    }

    public function deleteWithId($id) {
        if(NewsletterPost::destroy($id)){
            return true;
        }
        return false;
    }

    public function deleteWithSlug($slug) {
        
    }

    public function edit($data) {
        
    }

    public function getAll() {
        return NewsletterPost::all();
    }

    public function getAllForSelect() {
        
    }

    public function getMessages() {
        return [
            'required' => 'Forneça um email válido.', 'max' => 'Limite o email a :max caracteres.', 'email' => 'Formato de email inválido.', 'unique' => 'Este email já se encontra cadastrado em nossa newsletter.'
        ];
    }

    public function getRules($type, $parameters) {
        return [
            'email' => 'required|max:255|email|unique:newsletters_post,email'
        ];
    }

    public function getWithId($id) {
        
    }

    public function getWithSlug($slug) {
        
    }

    public function getMessageReturn($slug) {
        $messages = '{
                "SolicitationError":"Desculpe algo deu errado, tente novamente mais tarde.",
                "RegisterSuccess":"Seu email foi registrado com sucesso.",
                "DeleteNewLetterSuccess":"O emai foi excluído."
                }';
        $json = json_decode($messages);
        return $json->$slug;
    }

}
