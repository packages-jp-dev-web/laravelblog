<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlogJp\Classes\Services;

//Services
use UtilitiesJp\Services\ServiceDefault;
//Utilities
use UtilitiesJp\ClassesUtilitys\LogsSystem;
//Models
use BlogJp\Classes\Models\LikePost;

/**
 * Description of LikePostService
 *
 * @author Jefferson
 */
class LikePostService extends LogsSystem implements ServiceDefault {

    /**
     * Create LikePost
     * @param array $data
     * @return LikePost|null
     */
    public function create($data) {
        return LikePost::create($data);
    }

    /**
     * Delete Like Post with id
     * @param int $id Id do LikePost
     * @return boolean
     */
    public function deleteWithId($id) {
        if (LikePost::where('id', $id)->delete()) {
            return true;
        }
        return false;
    }

    /**
     * Delete Like Post with slug
     * @param string $slug Slug of LikePost
     * @return boolean
     */
    public function deleteWithSlug($slug) {
        if (LikePost::where('slug', $slug)->delete()) {
            return true;
        }
        return false;
    }

    public function edit($data) {
        
    }

    public function getAll() {
        
    }

    public function getAllForSelect() {
        
    }

    public function getMessages() {
        
    }

    public function getRules($type, $parameters) {
        
    }

    public function getWithId($id) {
        
    }

    public function getWithSlug($slug) {
        
    }
    
    public function getMessageReturn($slug) {
        $messages = '{
                "LikeSuccess":"Opção registrada."
                }';
        $json = json_decode($messages);
        return $json->$slug;
    }

    /**
     * Return LikePost per post and ip
     * @param int $post_id Id do Post
     * @param string $ip ip of visitor
     * @return LikePost|null
     */
    public function getPerPostAndIp($post_id, $ip) {
        return LikePost::where('post_id', $post_id)->where('ip', $ip)->first();
    }

    /**
     * Verify if exist like with post and ip
     * @param int $post_id Id do Post
     * @param string $ip ip of visitor
     * @return boolean
     */
    public function verifyIfExistLike($post_id, $ip) {
        if (LikePost::where('post_id', $post_id)->where('ip', $ip)->count() > 0) {
            return true;
        }
        return false;
    }
      

}
