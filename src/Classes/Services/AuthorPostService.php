<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlogJp\Classes\Services;

//Utilities
use UtilitiesJp\ClassesUtilitys\LogsSystem;
use UtilitiesJp\ClassesUtilitys\StringUtility;
//Services
use UtilitiesJp\Services\ServiceDefault;
//Models
use BlogJp\Classes\Models\AuthorPost;

/**
 * Description of AuthorService
 *
 * @author Jefferson
 */
class AuthorPostService extends LogsSystem implements ServiceDefault {

    //put your code here
    public function create($data) {
        $data['slug'] = StringUtility::generateSlugOfTextWithComplement('autor-' . time() . rand(1111, 9999));
        return AuthorPost::create($data);
    }

    public function edit($data) {
        
    }

    public function getWithId($id) {
        
    }

    public function getWithSlug($slug) {
        
    }

    public function getPerUserId($user_id) {
        return AuthorPost::where('user_id', $user_id)->first();
    }

    public function getAll() {
        
    }

    public function getAllForSelect() {
        
    }

    public function deleteWithId($id) {
        
    }

    public function deleteWithSlug($slug) {
        
    }

    public function getMessages() {
        
    }

    public function getRules($type, $parameters) {
        
    }

    //Aux

    public function verifyIfExistUserForAuthor($user_id) {
        if (AuthorPost::where('user_id', $user_id)->count() > 0) {
            return true;
        }
        return false;
    }

}
