<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlogJp\Classes\Services;

//Utilities
use UtilitiesJp\ClassesUtilitys\LogsSystem;
use UtilitiesJp\ClassesUtilitys\StringUtility;
use UtilitiesJp\ClassesUtilitys\ArrayUtility;
//Services
use UtilitiesJp\Services\ServiceDefault;
//Models
use BlogJp\Classes\Models\CategoryPost;
//Default
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Description of CategoryService
 *
 * @author Jefferson
 */
class CategoryPostService extends LogsSystem implements ServiceDefault {

    //put your code here
    /**
     * Create category
     * @param array $data Data of Category [name,description]
     * @return CategoryPost|null
     */
    public function create($data) {
        $data['slug'] = StringUtility::generateSlugOfTextWithComplement($data['name']);
        return CategoryPost::create($data);
    }

    /**
     * Edit Category
     * @param type $data Data of Category [id,name,description]
     * @return CategoryPost|null
     */
    public function edit($data) {
        try {
            $data['slug'] = StringUtility::generateSlugOfTextWithComplement($data['name']);
            $category = CategoryPost::findOrFail($data['id']);
            if ($category->update($data)) {
                return $category;
            }
            return null;
        } catch (ModelNotFoundException $ex) {
            $this->writeLog($ex->getCode() . ': Line = ' . $ex->getLine() . ' - ' . $ex->getMessage());
            return null;
        }
    }

    public function deleteWithId($id) {
        
    }

    /**
     *  Delete CategoryPost for slug
     * @param string $slug Slug of CategoryPost
     * @return boolean
     */
    public function deleteWithSlug($slug) {
        if (CategoryPost::where('slug', $slug)->delete()) {
            return true;
        }
        return false;
    }

    /**
     * Return all Categories
     * @return CategoryPost[]
     */
    public function getAll() {
        return CategoryPost::all();
    }

    public function getAllForSelect() {
        return ArrayUtility::convertArrayForInputSelect('id','name', CategoryPost::all());
    }

    /**
     * Return custom messages of validation CategoryPost
     * @return array
     */
    public function getMessages() {
        return [
            'name.required' => 'Defina um nome para sua categoria',
            'description.required' => 'Descreva em poucas palavras essa categoria',
            'name.max' => 'Limite o nome da categoria a :max caracteres.',
            'description.max' => 'Escreva uma descrição de no máximo :max caracteres.',
            'name.unique' => 'Já exite uma categoria com esse nome, tente outro.'
        ];
    }

    /**
     * Return Rules Validation CategoryPost
     * @param string $type [create,edit]
     * @param array $parameters
     * @return array
     */
    public function getRules($type, $parameters) {
        $result = [];
        switch ($type) {
            case 'create':
                $result = [
                    'name' => 'required|max:30|unique:category_posts_jp,name',
                    'description' => 'required|max:255'
                ];
                break;
            case 'edit':
                $result = [
                    'name' => 'required|max:30|unique:category_posts_jp,name,'.$parameters['id'],
                    'description' => 'required|max:255'
                ];
                break;
        }
        return $result;
    }

    public function getWithId($id) {
        
    }

    /**
     * Return CategoryPost for slug
     * @param string $slug Slug of CategoryPost
     * @return CategoryPost|null
     */
    public function getWithSlug($slug) {
        return CategoryPost::where('slug', $slug)->first();
    }

    public function getMessageReturn($slug) {
        $messages = '{
                "SolicitationError":"Desculpe algo deu errado, tente novamente mais tarde.",
                "RegisterCategorySuccess":"Sua nova categoria foi registrada.",                
                "EditCategorySuccess":"Dados da categoria atualizados.",
                "DeleteCategorySuccess":"Categoria excluída."
                }';
        $json = json_decode($messages);
        return $json->$slug;
    }

}
