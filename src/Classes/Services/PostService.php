<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlogJp\Classes\Services;

//Services
use UtilitiesJp\Services\ServiceDefault;
//Utilities
use UtilitiesJp\ClassesUtilitys\LogsSystem;
use UtilitiesJp\ClassesUtilitys\StringUtility;
use UtilitiesJp\ClassesUtilitys\FileUtility;
//Models
use BlogJp\Classes\Models\Post;
use BlogJp\Classes\Models\VisitPost;
//Default
use Illuminate\Database\Eloquent\ModelNotFoundException;
use BlogJp\Classes\Utility\Encoding;

/**
 * Description of PostService
 *
 * @author Jefferson
 */
class PostService extends LogsSystem implements ServiceDefault {

    /**
     * Create Post
     * @param array $data !(optional) [author_id,category_id,title,description,content,!status]
     * @return Post|null
     */
    public function create($data) {
        $data['slug'] = StringUtility::generateSlugOfTextWithComplement($data['title']);
        return Post::create($data);
    }

    /**
     * Edit Post
     * @param array $data !(optional) [id,author_id,category_id,title,description,content,!status]
     * @return Post|null
     */
    public function edit($data) {
        try {
            $post = Post::findOrFail($data['id']);
            $data['slug'] = StringUtility::generateSlugOfTextWithComplement($data['title']);
            if ($post->update($data)) {
                return $post;
            }
            return null;
        } catch (ModelNotFoundException $ex) {
            $this->writeLog($ex->getCode() . ': Line = ' . $ex->getLine() . ' - ' . $ex->getMessage());
            return null;
        }
    }

    public function deleteWithId($id) {
        
    }

    /**
     * Delete Post per slug
     * @param string $slug Slug of post
     * @return boolean
     */
    public function deleteWithSlug($slug) {
        if (Post::where('slug', $slug)->delete()) {
            return true;
        }
        return false;
    }

    /**
     * Return all Post
     * @return Post[]
     */
    public function getAll() {
        return Post::all();
    }

    public function getAllForSelect() {
        
    }

    /**
     * Return all post per order column and optional amount
     * @param string $order orientation order
     * @param string $column column per ordering
     * @param int $take amount of Post return
     * @param int $paginate amount of Post per page
     * @return Post[]
     */
    public function getAllActiveOrderTake($order, $column, $take, $paginate) {
        if (!$order) {
            $order = 'desc';
        }
        if ($take) {
            if ($paginate) {
                return Post::where('status', true)->orderBy($column, $order)->take($take)->paginate($paginate);
            }
            return Post::where('status', true)->orderBy($column, $order)->take($take)->get();
        }
        if ($paginate) {
            return Post::where('status', true)->orderBy($column, $order)->paginate($paginate);
        }
        return Post::where('status', true)->orderBy($column, $order)->get();
    }

    /**
     * Return all post per order column and optional amount and category
     * @param string $order orientation order
     * @param string $column column per ordering
     * @param int $take amount of Post return
     * @param int $paginate amount of Post per page
     * @param int $category_id Id da categoria
     * @return Post[]
     */
    public function getAllActiveOrderTakeCategory($order, $column, $take, $paginate, $category_id) {
        if (!$order) {
            $order = 'desc';
        }
        if ($take) {
            if ($paginate) {
                return Post::where('status', true)->where('category_id', $category_id)->orderBy($column, $order)->take($take)->paginate($paginate);
            }
            return Post::where('status', true)->where('category_id', $category_id)->orderBy($column, $order)->take($take)->get();
        }
        if ($paginate) {
            return Post::where('status', true)->where('category_id', $category_id)->orderBy($column, $order)->paginate($paginate);
        }
        return Post::where('status', true)->where('category_id', $category_id)->orderBy($column, $order)->get();
    }

    public function getMessages() {
        return [
            'title.required' => 'Forneça um título para o post.',
            'title.unique' => 'Você já cadastrou um post com esse título, tente outro.',
            'category_id.required' => 'O post necessita de uma categoria.',
            'description.required' => 'A descrição é necessária para apresentação do post ao visitante.',
            'description.max' => 'A descrição se limita a :max caracteres.',
            'content.required' => 'Entre o conteúdo do post para publicá-lo.'
        ];
    }

    public function getRules($type, $parameters) {
        $result = [];
        switch ($type) {
            case 'create':
                $result = [
                    'title' => 'required|max:255|unique:posts_jp,title',
                    'category_id' => 'required',
                    'description' => 'required|max:400',
                    'content' => 'required'
                ];
                break;
            case 'edit':
                $result = [
                    'title' => 'required|max:255|unique:posts_jp,title,' . $parameters['id'],
                    'category_id' => 'required',
                    'description' => 'required|max:400',
                    'content' => 'required'
                ];
                break;
        }
        return $result;
    }

    public function getWithId($id) {
        
    }

    /**
     * Return post per slug
     * @param string $slug Slug of post
     * @return Post|null
     */
    public function getWithSlug($slug) {
        return Post::where('slug', $slug)->first();
    }

    /**
     * Register visit the post
     * @param int $post_id Id of Post
     * @param string $ip Ip of User
     * @return VisitPost|null
     */
    public function registerVisit($post_id, $ip) {
        return VisitPost::firstOrCreate(['post_id' => $post_id, 'ip' => $ip]);
    }

    public function getMessageReturn($slug) {
        $messages = '{
                "SolicitationError":"Desculpe algo deu errado, tente novamente mais tarde.",
                "RegisterPostSuccess":"Seu post foi criado com sucesso.",                
                "EditPostSuccess":"Os dados do seu post foram atualizados.",
                "DeletePostSuccess":"Post excluído."
                }';
        $json = json_decode($messages);
        return $json->$slug;
    }

    /**
     * Upload of images of input content post
     * @param string $context
     * @return string
     */
    public function uploadImagesContent($context) {
        $dom = new \domdocument('1.0', 'utf-8');
        @$dom->loadHtml(mb_convert_encoding($context, 'HTML-ENTITIES', 'UTF-8'));
        $dom->removeChild($dom->doctype);
        //@$dom->loadHtml(mb_convert_encoding ( $context ,  'HTML-ENTITIES' ,  'UTF-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');
        foreach ($images as $k => $img) {
            $data = $img->getattribute('src');
            $dataDivider = explode(';', $data);
            if (count($dataDivider) > 1) {
//list$type, $data) = explode(';', $data);
                $type = $dataDivider[0];
                $contentImg = $dataDivider[1];
                list(, $imgInCharacter) = explode(',', $contentImg);
                list(, $typemin) = explode('/', $type);
                $contentImgSave = base64_decode($imgInCharacter);
                $image_name = StringUtility::generateSlugOfTextWithComplement($k . time()) . '.' . $typemin;

                $path = public_path() . '/images/post/' . $image_name;
                file_put_contents($path, $contentImgSave);

                $img->removeattribute('src');
                $img->setattribute('src', '/images/post/' . $image_name);
            }
        }
        return $dom->saveHTML();
    }

    public function getImagesContent($context) {
        $dom = new \domdocument();
        @$dom->loadHtml($context, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getelementsbytagname('img');
        $result = [];
        foreach ($images as $k => $img) {
            $result[] = $img->getattribute('src');
        }
        return $result;
    }

    public function removeImageUnnecessary($imagesBefore, $imagesAfter) {
        $imagesDelete = array_diff($imagesBefore, $imagesAfter);
        foreach ($imagesDelete as $img) {
            FileUtility::deleteFileDirectoryPublic($img);
        }
    }

}
