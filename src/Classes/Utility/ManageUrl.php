<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace BlogJp\Classes\Utility;

/**
 * Description of ManageUrl
 *
 * @author Jefferson
 */
class ManageUrl {

    public static function category($slug) {
        return ManageUrl::urlcategory(1, $slug);
    }

    public static function categoryNotPrefix($slug) {
        return ManageUrl::urlcategory(2, $slug);
    }

    public static function urlcategory($type, $slug) {
        $result = '';
        if ($type == 1) {
            $result = '/blog';
        }
        switch ($slug) {
            case 'list':
                $result .= '/listar-categorias-do-blog/';
                break;
            case 'edit':
                $result .= '/editar-categoria-do-blog/';
                break;
            case 'create':
                $result .= '/criar-categoria-do-blog/';
                break;
            case 'delete':
                $result .= '/excluir-categoria-do-blog/';
                break;
        }
        return $result;
    }

    public static function post($slug) {
        return ManageUrl::urlpost(1, $slug);
    }

    public static function postNotPrefix($slug) {
        return ManageUrl::urlpost(2, $slug);
    }

    public static function urlpost($type, $slug) {
        $result = '';
        if ($type == 1) {
            $result = '/blog';
        }
        switch ($slug) {
            case 'list':
                $result .= '/listar-posts-do-blog/';
                break;
            case 'edit':
                $result .= '/editar-post-do-blog/';
                break;
            case 'create':
                $result .= '/criar-post-do-blog/';
                break;
            case 'delete':
                $result .= '/excluir-post-do-blog/';
                break;
            case 'like-post':
                $result .= '/curtir-post/';
                break;
            case 'newsletter':
                $result .= '/registrar-email-newsletter/';
                break;
             case 'list-newsletter':
                $result .= '/listar-emails-newsletter/';
                break;
            case 'delete-newsletter':
                $result .= '/excluir-email-newsletter/';
                break;
             case 'cancel-newsletter':
                $result .= '/cancelar-email-newsletter/';
                break;
        }
        return $result;
    }

}
