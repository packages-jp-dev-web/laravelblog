<?php

namespace BlogJp\Classes\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailAfterCreatePost extends Mailable {

    use Queueable,
        SerializesModels;

    public $post, $newsletter;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data) {
        $this->post = $data['post'];
        $this->newsletter = $data['newsletter'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->markdown('blogjp::email.email-after-register-post', [
                            'context' => view('blogjp::email.context.context-email-after-register-post', ['post' => $this->post])->render(),
                            'newsletter' => $this->newsletter,
                            'header_url' => url('/'),
                            'url' => url('/'),
                            'header_title' => ''
                        ])
                        ->subject('Blog - ' . env('APP_NAME'));
    }

}
