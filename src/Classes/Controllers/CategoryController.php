<?php

namespace BlogJp\Classes\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Services
use BlogJp\Classes\Services\CategoryPostService;
//Utilities
use BlogJp\Classes\Utility\ManageUrl;

class CategoryController extends Controller {

    public function getRegister() {
        return view('blogjp::category.createoredit', ['category' => null]);
    }

    public function postRegister(Request $request, CategoryPostService $categoryPostService) {
        $this->validate($request, $categoryPostService->getRules('create', null), $categoryPostService->getMessages());
        if ($categoryPostService->create($request->all())) {
            return redirect()->back()->with('success', $categoryPostService->getMessageReturn('RegisterCategorySuccess'));
        }
        return redirect()->back()->with('error', $categoryPostService->getMessageReturn('SolicitationError'));
    }

    public function getEdit(CategoryPostService $categoryPostService,$slug) {
        return view('blogjp::category.createoredit',['category'=>$categoryPostService->getWithSlug($slug)]);
    }

    public function postEdit(Request $request, CategoryPostService $categoryPostService) {
        $categoryPost = $categoryPostService->getWithSlug($request->slug);
        $this->validate($request, $categoryPostService->getRules('edit', ['id'=>$categoryPost->id]), $categoryPostService->getMessages());
        if (!$categoryPost) {
            return redirect()->back()->with('error', $categoryPostService->getMessageReturn('SolicitationError'));
        }
        $data = $request->all();
        $data['id'] = $categoryPost->id;
        $categoryChain = $categoryPostService->edit($data);
        if ($categoryChain) {
            return redirect()->to(ManageUrl::category('edit').$categoryChain->slug)->with('success', $categoryPostService->getMessageReturn('EditCategorySuccess'));
        }
        return redirect()->back()->with('error', $categoryPostService->getMessageReturn('SolicitationError'));
    }

    public function getList(CategoryPostService $categoryPostService) {
        return view('blogjp::category.list', ['categories' => $categoryPostService->getAll()]);
    }
    
    public function getDelete(CategoryPostService $categoryPostService,$slug){
        if($categoryPostService->deleteWithSlug($slug)){
            return redirect()->back()->with('success', $categoryPostService->getMessageReturn('DeleteCategorySuccess'));
        }
        return redirect()->back()->with('error', $categoryPostService->getMessageReturn('SolicitationError'));
    }

}
