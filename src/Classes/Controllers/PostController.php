<?php

namespace BlogJp\Classes\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//Services
use BlogJp\Classes\Services\PostService;
use BlogJp\Classes\Services\CategoryPostService;
use BlogJp\Classes\Services\AuthorPostService;
use BlogJp\Classes\Services\LikePostService;
use BlogJp\Classes\Services\NewsletterPostService;
//Utilities
use BlogJp\Classes\Utility\ManageUrl;
use UtilitiesJp\ClassesUtilitys\EncryptUtility;
//Jobs
use BlogJp\Classes\Jobs\Email\SendEmailAfterCreatePostJob;

class PostController extends Controller {

    public function getRegister(CategoryPostService $categoryPostService) {
        return view('blogjp::post.createoredit', ['post' => null, 'categories' => $categoryPostService->getAllForSelect()]);
    }

    public function postRegister(Request $request, PostService $postService, AuthorPostService $authorPostService) {
        $this->validate($request, $postService->getRules('create', null), $postService->getMessages());
        $data = $request->all();
        $data['content'] = $postService->uploadImagesContent($data['content']);
        $data['author_id'] = $authorPostService->getPerUserId(auth()->user()->id)->id;
        if ($post = $postService->create($data)) {
            $email_post = config('blogjp.email_post');
            if ($email_post) {
                SendEmailAfterCreatePostJob::dispatch($post);
            }
            return redirect()->back()->with('success', $postService->getMessageReturn('RegisterPostSuccess'));
        }
        return redirect()->back()->with('error', $postService->getMessageReturn('SolicitationError'));
    }

    public function getEdit(PostService $postService, CategoryPostService $categoryPostService, $slug) {
        return view('blogjp::post.createoredit', ['post' => $postService->getWithSlug($slug), 'categories' => $categoryPostService->getAllForSelect()]);
    }

    public function postEdit(Request $request, PostService $postService) {
        $post = $postService->getWithSlug($request->slug);
        $this->validate($request, $postService->getRules('edit', ['id' => $post->id]), $postService->getMessages());
        $data = $request->all();
        $data['id'] = $post->id;
        $imagesBefore = $postService->getImagesContent($post->content);
        $imagesAfter = $postService->getImagesContent($data['content']);
        $data['content'] = $postService->uploadImagesContent($data['content']);
        $postService->removeImageUnnecessary($imagesBefore, $imagesAfter);
        $postChain = $postService->edit($data);
        if ($postChain) {
            return redirect()->to(ManageUrl::post('edit') . $postChain->slug)->with('success', $postService->getMessageReturn('EditPostSuccess'));
        }
        return redirect()->back()->with('error', $postService->getMessageReturn('SolicitationError'));
    }

    public function getList(PostService $postService) {
        return view('blogjp::post.list', ['posts' => $postService->getAll()]);
    }

    public function getDelete(PostService $postService, $slug) {
        if ($postService->deleteWithSlug($slug)) {
            return redirect()->back()->with('success', $postService->getMessageReturn('DeletePostSuccess'));
        }
        return redirect()->back()->with('error', $postService->getMessageReturn('SolicitationError'));
    }

    public function getLike(Request $request, PostService $postService, LikePostService $likePostService) {
        $post = $postService->getWithSlug($request->slug);
        $likePost = $likePostService->getPerPostAndIp($post->id, request()->ip());
        if ($likePost) {
            $likePostService->deleteWithId($likePost->id);
            $operation = 'unlike';
        } else {
            $likePostService->create(['post_id' => $post->id, 'ip' => request()->ip()]);
            $operation = 'like';
        }
        if (request()->ajax()) {
            return response()->json(['result' => 'success',
                        'message' => $likePostService->getMessageReturn('LikeSuccess'),
                        'operation' => $operation,
                        'slugReturn' => $request->slugReturn,
                        'countLike' => $post->likePost->count()
            ]);
        }
        return redirect()->back()->with('success', $likePostService->getMessageReturn('LikeSuccess'))->with('operation', $operation);
    }

    //NewsLetter
    public function getListNewsLetter(NewsletterPostService $newsLetterPostService) {
        return view('blogjp::newsletter.list', ['newsletters' => $newsLetterPostService->getAll()]);
    }

    public function getDeleteNewsLetter(NewsletterPostService $newsLetterPostService, $id) {
        if ($newsLetterPostService->deleteWithId($id)) {
            return redirect()->back()->with('success', $newsLetterPostService->getMessageReturn('DeleteNewLetterSuccess'));
        }
        return redirect()->back()->with('error', $newsLetterPostService->getMessageReturn('SolicitationError'));
    }

    public function getCancelNewsLetter(NewsletterPostService $newsLetterPostService, $id) {
        $idChain = EncryptUtility::numHash($id);
        if ($newsLetterPostService->deleteWithId($idChain)) {
            return 'Seu email foi excluído de nossa newsletter.';
        }
        return 'Email ausente em nossa newsletter.';
    }

    public function postRegisterNewsletter(Request $request, NewsletterPostService $newsLetterPostService) {
        $data = $request->all();
        $validator = validator()->make($data, $newsLetterPostService->getRules(null, null), $newsLetterPostService->getMessages());
        if ($validator->fails()) {
            return response()->json(['result' => 'fails', 'errors' => ['email' => $validator->errors()->first('email')]]);
        }
        if ($newsLetterPostService->create($data)) {
            return response()->json(['result' => 'success', 'message' => $newsLetterPostService->getMessageReturn('RegisterSuccess')]);
        }
        return response()->json(['result' => 'error', 'message' => $newsLetterPostService->getMessageReturn('SolicitationError')]);
    }

}
