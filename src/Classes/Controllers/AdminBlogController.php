<?php

namespace BlogJp\Classes\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminBlogController extends Controller
{
    public function getLogin(){
        return view('admin.login');
    }
    public function getHome(){
        return view('admin.index');
    }
}
