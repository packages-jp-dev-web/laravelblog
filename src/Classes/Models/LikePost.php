<?php

namespace BlogJp\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class LikePost extends Model
{
    protected $table = 'like_posts_jp';
    protected $fillable = ['post_id','ip'];
    public $timestamps = false;
}
