<?php

namespace BlogJp\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class VisitPost extends Model
{
    protected $table = 'visit_posts';
    protected $fillable =['post_id','ip'];
    public $timestamps = true;
}
