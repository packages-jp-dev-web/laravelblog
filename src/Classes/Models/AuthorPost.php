<?php

namespace BlogJp\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class AuthorPost extends Model
{
    protected $table = 'author_posts_jp';
    protected $fillable = ['slug','user_id','description','curriculum'];
    public $timestamps = false;
    
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
