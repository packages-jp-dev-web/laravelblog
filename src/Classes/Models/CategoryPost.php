<?php

namespace BlogJp\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryPost extends Model {
    //name: max=30 description: max=255
    protected $table = 'category_posts_jp';
    protected $fillable = ['slug','name','description'];
    public $timestamps = true;
}
