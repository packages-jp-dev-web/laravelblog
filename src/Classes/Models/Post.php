<?php

namespace BlogJp\Classes\Models;

use Illuminate\Database\Eloquent\Model;
//Utilities
use UtilitiesJp\ClassesUtilitys\DateUtility;

class Post extends Model {

    protected $table = 'posts_jp';
    protected $fillable = ['slug', 'author_id', 'category_id', 'title', 'description', 'content', 'status'];
    public $timestamps = true;

    public function date(){
        return date('Y - m - d', strtotime($this->created_at));
    }

    public function dateCreate() {
        return date('d/m/Y', strtotime($this->created_at));
    }

    public function author() {
        return $this->belongsTo('BlogJp\Classes\Models\AuthorPost', 'author_id');
    }

    public function category() {
        return $this->belongsTo('BlogJp\Classes\Models\CategoryPost', 'category_id');
    }
    
    public function likePost() {
        return $this->hasMany('BlogJp\Classes\Models\LikePost', 'post_id');
    }
    
    public function visits(){
        return $this->hasMany('BlogJp\Classes\Models\VisitPost', 'post_id');
    }

}
