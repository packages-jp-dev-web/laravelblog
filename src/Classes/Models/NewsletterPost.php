<?php

namespace BlogJp\Classes\Models;

use Illuminate\Database\Eloquent\Model;

class NewsletterPost extends Model
{
    protected $table = 'newsletters_post';
    protected $fillable = ['email'];
    public $timestamps = true;
}
