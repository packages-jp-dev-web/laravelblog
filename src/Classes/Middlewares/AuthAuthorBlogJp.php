<?php

namespace BlogJp\Classes\Middlewares;

use Closure;
use BlogJp\Classes\Services\AuthorPostService;
use App\Utility\ManageUrl;
class AuthAuthorBlogJp {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (!auth()->check()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                            'result' => 'error',
                            'message' => 'Refaça o login e tente novamente.']);
            } else {
                return redirect()->to(ManageUrl::admin('home'))->with('error', 'Refaça o login e tente novamente.');
            }
        }
        $authorPostService = new AuthorPostService();
        if (!$authorPostService->verifyIfExistUserForAuthor(auth()->user()->id)) {
            if ($request->ajax() || $request->wantsJson()) {
                return response()->json([
                            'result' => 'error',
                            'message' => 'Você não possui acesso de autor.']);
            } else {
                return redirect()->to(ManageUrl::admin('home'))->with('error', 'Você não possui acesso de autor.');
            }
        }
        return $next($request);
    }

}
