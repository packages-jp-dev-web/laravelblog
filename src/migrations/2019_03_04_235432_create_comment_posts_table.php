<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentPostsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('comment_posts_jp', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                    ->references('id')
                    ->on('users')
                    ->onDelete('cascade');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')
                    ->references('id')
                    ->on('posts_jp')
                    ->onDelete('cascade');
            $table->integer('comment_id')->unsigned()->nullable()->default(null);
            $table->foreign('comment_id')
                    ->references('id')
                    ->on('comment_posts_jp')
                    ->onDelete('cascade');
            $table->string('message', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('comment_posts_jp');
    }

}
